#pragma once

#pragma warning(disable: 4251)
#pragma comment(lib,"winmm.lib")
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")


#ifdef LITEHV
#define LITEHV_EXTERN __declspec(dllexport)
#else
#define LITEHV_EXTERN __declspec(dllimport)
#pragma comment(lib,"litehv.lib")
#endif
