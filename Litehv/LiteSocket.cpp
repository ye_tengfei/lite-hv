#include "pch.h"
#include "LiteSocket.h"
#include <Thread>
#include <mutex>

class CSocketObj
{
public:
	CSocketObj()
	{
		m_loop = hloop_new(0);
		m_listenio = nullptr;
		m_bServer = false;
	}
	~CSocketObj()
	{
		if (m_loop)
		{
			hloop_free(&m_loop);
		}

	}
	int m_status;
	bool m_bServer;	//是否为服务器
	hloop_t* m_loop;
	hio_t*  m_listenio;
	hio_t*  m_client;
	std::mutex m_lock;
};

CLiteSocket::CLiteSocket()
{
	m_SocketObj = new CSocketObj();
}

CLiteSocket::~CLiteSocket()
{
	if (m_SocketObj != NULL)
	{
		delete m_SocketObj;
		m_SocketObj = NULL;
	}
}

void OnCloseUV(hio_t* io)
{
	CLiteSocket* pSocket = (CLiteSocket*)hevent_userdata(io);
	pSocket->OnClose();
}

void OnReceiveUV(hio_t* io, void* buf, const int  readbytes)
{
	
	CLiteSocket* pSocket = (CLiteSocket*)hevent_userdata(io);
	if (readbytes > 0)
	{
		pSocket->OnReceive((char*)buf, readbytes);
	}
	else if (readbytes == 0)
	{
		pSocket->Close();
	}
	else
	{
		pSocket->Close();
	}
}


void OnAcceptUV(hio_t* io)
{
	CLiteSocket* pSocketServer = (CLiteSocket*)hevent_userdata(io);
	pSocketServer->m_SocketObj->m_client = io;
	pSocketServer->OnConnent(pSocketServer);
}

void OnConnectUV(hio_t* io)
{
	CLiteSocket* pSocket = (CLiteSocket*)hevent_userdata(io);
	pSocket->OnConnent(pSocket);
	// 设置close回调
	hio_setcb_close(io, OnCloseUV);
	// 设置read回调
	hio_setcb_read(io, OnReceiveUV);
	// 开始读
	hio_read(io);
}

void RunThreadUV(void* arg)
{
	CLiteSocket* pSocket = (CLiteSocket*)arg;
	hloop_run(pSocket->m_SocketObj->m_loop);
}


void RunThreadClientUV(void* arg)
{
	CLiteSocket* pSocket = (CLiteSocket*)arg;
	hloop_run(pSocket->m_SocketObj->m_loop);

}




void CLiteSocket::OnConnent(CLiteSocket* client)
{
}

void CLiteSocket::OnReceive(char* buf, unsigned int buflen)
{
}


void CLiteSocket::OnClose()
{
}


int CLiteSocket::CreateServer(char* strAddress, int nPort)
{
	hio_t* listenio = hloop_create_tcp_server(m_SocketObj->m_loop, strAddress, nPort, OnAcceptUV);
	if (listenio == nullptr) {
		return -1;
	}
	m_SocketObj->m_bServer = true;
	hevent_set_userdata(listenio,this);
	m_SocketObj->m_listenio = listenio;
	// 运行事件循环
	//hloop_run(m_SocketObj->m_loop);
	hthread_create((hthread_routine)RunThreadUV, this);
	return 0;

}

int CLiteSocket::Accept(CLiteSocket* pSockets, CLiteSocket* pSocketc)
{
	printf("CLiteSocket::Accept\n");
	pSocketc->m_SocketObj->m_listenio = pSockets->m_SocketObj->m_client;
	hevent_set_userdata(pSocketc->m_SocketObj->m_listenio, pSocketc);
	// 设置read回调
	hio_setcb_read(pSocketc->m_SocketObj->m_listenio, OnReceiveUV);//拿到server onAccept中的hio_t
	// 开始读
	hio_read(pSocketc->m_SocketObj->m_listenio);
	return 0;
}

int CLiteSocket::ConnectServer(char* strAddress, int nPort)
{
	hio_t* listenio = hloop_create_tcp_client(m_SocketObj->m_loop, strAddress, nPort, OnConnectUV);
	if (listenio == nullptr) {
		return -1;
	}
	hevent_set_userdata(listenio, this);
	m_SocketObj->m_listenio = listenio;
	// 运行事件循环
	//hloop_run(m_SocketObj->m_loop);
	hthread_create((hthread_routine)RunThreadClientUV, this);
	return 0;
}

int CLiteSocket::Send(char* buf, unsigned int buflen)
{
	if (buf == NULL || buflen == 0)
		return -1;
	if (!m_SocketObj->m_listenio)
		return -1;
	int nWriteLien = hio_write(m_SocketObj->m_listenio, buf, buflen);
	return nWriteLien;
}

void CLiteSocket::Close()
{
	
}