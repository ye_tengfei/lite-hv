#pragma once
#include "../libhv-master/build/include/hv/hloop.h"
#include "../libhv-master/build/include/hv/hsocket.h"
#include "../libhv-master/build/include/hv/hssl.h"
#include "../libhv-master/build/include/hv/hthread.h"
#include "LiteHV.h"
#include <stdlib.h>
#include <vector>
#include <list>
#include <memory>


class CSocketObj;
class LITEHV_EXTERN CLiteSocket
{
public:
	CLiteSocket();
	virtual ~CLiteSocket();
	virtual void OnConnent(CLiteSocket* client);
	virtual void OnReceive(char* buf, unsigned int buflen);
	virtual void OnClose();



	int CreateServer(char* strAddress, int nPort);
	int Accept(CLiteSocket* pSockets, CLiteSocket* pSocketc);
	int ConnectServer(char* strAddress, int nPort);
	int Send(char* buf, unsigned int buflen);
	void Close();
	CSocketObj* m_SocketObj;
private:
	BYTE* m_pBrokenIns;
	int m_BrokenInsSize;
};