#pragma once
#include "../Litehv/LiteSocket.h"
class ServerSocket :public CLiteSocket
{
public:
	ServerSocket();
	~ServerSocket();

	virtual void OnConnent(CLiteSocket* server) override;

	virtual void OnClose() override;
};

