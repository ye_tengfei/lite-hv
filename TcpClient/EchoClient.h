#pragma once
#include "../Litehv/LiteSocket.h"
class EchoClient :public CLiteSocket
{
public:
	EchoClient();
	virtual ~EchoClient();
	virtual void OnConnent(CLiteSocket* client) override;

	virtual void OnReceive(char* buf, unsigned int buflen) override;

	virtual void OnClose() override;

	virtual void SendMsg(int type, const char* buf, unsigned int buflen);
};

